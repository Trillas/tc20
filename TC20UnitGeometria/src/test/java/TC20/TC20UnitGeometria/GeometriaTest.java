package TC20.TC20UnitGeometria;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

class GeometriaTest {
	
	Geometria cuadrado = new Geometria(1);
	Geometria circulo = new Geometria(2);
	Geometria triangulo = new Geometria(3);
	Geometria rectangula = new Geometria(4);
	Geometria pentagono = new Geometria(5);
	Geometria rombo = new Geometria(6);
	Geometria romboide = new Geometria(7);
	Geometria trapecio = new Geometria(8);
	Geometria defau = new Geometria();
	
	@Test
	public void testAreaCuadrado() {
		int resultado = Geometria.areacuadrado(2);
		int esperado = 4;
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testAreaCirculo() {
		double resultado =  Geometria.areaCirculo(4);
		double esperado = 50.2656;
		assertEquals(esperado, resultado, 1);
	}
	
	@Test
	public void testAreaTriangulo() {
		int resultado = Geometria.areatriangulo(2,3);
		int esperado = 3;
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testAreaRectangulo() {
		int resultado = Geometria.arearectangulo(2, 4);
		int esperado = 8;
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testAreaPentagono() {
		int resultado = Geometria.areapentagono(2, 4);
		int esperado = 4;
		assertEquals(esperado, resultado);
	}

	@Test
	public void testAreaRombo() {
		int resultado = Geometria.arearombo(2, 4);
		int esperado = 4;
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testAreaRomboide() {
		int resultado = Geometria.arearomboide(2, 8);
		int esperado = 16;
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testAreaTrapecio() {
		int resultado = Geometria.areatrapecio(2, 4, 5);
		int esperado = 15;
		assertEquals(esperado, resultado);
	}

	@Test
	public void testGetId() {
		assertEquals(1, cuadrado.getId());
	}
	
	@Test
	public void testSetId() {
		cuadrado.setId(2);
		assertEquals(2, cuadrado.getId());
	}
	
	@Test
	public void testGetNom() {
		assertEquals("cuadrado", cuadrado.getNom());
	}
	
	@Test
	public void testSetNom() {
		cuadrado.setNom("triangulo");
		assertEquals("triangulo", cuadrado.getNom());
	}
	
	@Test
	public void testGetArea() {
		cuadrado.setArea(2);
		assertEquals(2, cuadrado.getArea(), 1);
	}
	
	@Test
	public void testSetArea() {
		assertEquals(1, cuadrado.getId());
	}
	
	@Test
	public void testToString() {
		assertEquals("Geometria [id=1, nom=cuadrado, area=0.0]", cuadrado.toString());
	}
}
